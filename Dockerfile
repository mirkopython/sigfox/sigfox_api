FROM python:3.10
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code

RUN pip install pipenv
COPY . /code/
#RUN pipenv install
#RUN pipenv shell
RUN pip install -r requirements.txt
CMD ["python", "./sigfox_api/manage.py", "runserver","0.0.0.0:8000"]
