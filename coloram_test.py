import colorama

print(colorama.Fore.MAGENTA + "ESTO ES MAGENTA")
print("TEST SI SE BORRO")
print(colorama.Fore.RESET)
print("ESTO DEBERIA SER NORMAL")
print(colorama.Back.LIGHTGREEN_EX + "Esto cambio el fondo")
print(colorama.Style.RESET_ALL)
print(colorama.Style.DIM)
print(colorama.Fore.GREEN +"DIM")

print(colorama.Style.BRIGHT)
print(colorama.Fore.GREEN +"BRIGHT")
print(colorama.Style.RESET_ALL)

print(colorama.Style.NORMAL)
print(colorama.Fore.GREEN +"NORMAL")
print(colorama.Style.RESET_ALL)

print(colorama.Style.NORMAL)

print("END")