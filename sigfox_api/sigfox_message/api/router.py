#from macpath import basename
from rest_framework.routers import DefaultRouter
from sigfox_message.api.views import Alive, MessageApiViewSet

router_message = DefaultRouter()

router_message.register(prefix='', basename='post', viewset=MessageApiViewSet)
router_message.register(prefix='alive', basename='post', viewset=Alive)