from email.message import Message
from rest_framework.serializers import ModelSerializer
from sigfox_message.models import Message

class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'