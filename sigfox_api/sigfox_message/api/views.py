from rest_framework.viewsets import ModelViewSet
from sigfox_message.models import Message
from sigfox_message.api.serializers import MessageSerializer


class Alive(ModelViewSet):
    
    pass
    #return "It's Alive"

class MessageApiViewSet(ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()