from ast import operator
#from turtle import title
from django.db import models
from django_countries.fields import CountryField


# Create your models here.
class Message(models.Model):
    device = models.CharField(max_length=7)
    data = models.CharField(max_length=24)
    date = models.DateTimeField(auto_now=True)
    seqNumber = models.IntegerField()
    operator = models.CharField(max_length=50)
    country = CountryField()




